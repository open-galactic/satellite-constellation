import math

heavenly_body_radius = {  # [km]
    "earth": 6371,
    "luna": 1737,
    "mars": 3390,
    "venus": 6052,
    "mercury": 2440,
    "sol": 695700,
    "jupiter": 69911,
    "saturn": 58232,
    "uranus": 25362,
    "neptune": 24622,
    "pluto": 1188,
}

heavenly_body_mass = {  # [kg]
    "earth": 5.972 * 10**24,
    "luna": 73.46 * 10**21,
    "mars": 641.71 * 10**21,
    "venus": 4867.5 * 10**21,
    "mercury": 330.11 * 10**21,
    "sol": 1.9885 * 10**30,
    "jupiter": 1.8982 * 10**27,
    "saturn": 5.6834 * 10**26,
    "uranus": 8.6810 * 10**25,
    "neptune": 1.02413 * 10**26,
    "pluto": 13.03 * 10**21,
}

heavenly_body_period = {  # [days]
    "earth": 1,
    "luna": 27.321661,
    "mars": 1.02595675,
    "venus": 243.0187,
    "mercury": 58.6462,
    "sol": 25.379995,
    "jupiter": 0.41007,
    "saturn": 0.426,
    "uranus": 0.71833,
    "neptune": 0.67125,
    "pluto": 6.38718,
}

constants = {
    "G": 6.67408 * 10 ** (-11),  # Gravitational constant [m^3 kg^-1 s^-2]
    "wE": 7.2921159 * 10 ** (-5),  # Earth angular velocity [rad/s]
    "J2E": 10826269 * 10 ** (-3),  # Earth J2 constant
}

SPACE_COCKPIT_BASE_URL = "https://spacecockpit.space/"
URL_LENGTH_LIMIT = 2083


def to_positive_angle(angle):
    """
    Convert angle to positive equivalent, if negative (e.g. -90 = 270)
    """
    return (angle + 360) if (angle < 0) else angle


def normalise_angle(angle):
    """
    Normalise angle between (0, 360) degrees
    """
    return (angle % 360) if abs(angle) > 360 else angle


def get_body_radius(focus_body):
    """
    Returns the radius of the body being orbited (focus_body) [km]
    """
    return heavenly_body_radius[focus_body.lower()]


def get_body_mass(focus_body):
    """
    Returns the mass of the body being orbited (focus_body) [kg]
    """
    return heavenly_body_mass[focus_body.lower()]


def get_body_period(focus_body):
    """
    Returns the period of the body being orbited (focus_body) [days]
    """
    return heavenly_body_period[focus_body.lower()]


def eccentricAnomaly(eccentricity, mean_anom, dec_places, _maxiter):
    """
    Calculate eccentric anomaly from eccentricity, mean anomaly, decimal places
    and the max number of iterations

    Args:
        eccentricity (float): eccentricity of the target orbit
        mean_anom (float): current mean anomaly of the satellite
        dec_places (int): number of digits to round the result to
        maxiter (int): maximum number of iterations for the loop to run

    Returns:
        eccentric_anomaly (float): eccentric anomaly of satellite
    """

    i = 0
    delta = math.pow(10, -dec_places)

    if eccentricity < 0.8:
        E = mean_anom
    else:
        E = mean_anom + math.sin(mean_anom)

    F = E - eccentricity * math.sin(E) - mean_anom

    while abs(F) > delta and i < _maxiter:
        E = E - F / (1.0 - (eccentricity * math.cos(E)))
        F = E - eccentricity * math.sin(E) - mean_anom
        i = i + 1

    return round(E * math.pow(10, dec_places)) / math.pow(10, dec_places)


def trueAnomaly(e, E, dp):
    """
    Calculate true anomaly from eccentricity and eccentric anomaly, to a specified
    number of decimal places.

    Args:
        e (float): eccentricity of the satellite's orbit.
        E (float): current eccentric anomaly of the satellite, in radians.
        dp (int): number of decimal places.

    Returns:
        mean_anomaly (float): eccentric anomaly of satellite, in radians.
    """
    phi = 2.0 * math.atan(math.sqrt((1.0 + e) / (1.0 - e)) * math.tan(E / 2.0))
    return round(phi * math.pow(10, dp)) / math.pow(10, dp)


def meanAnomaly(true_anomaly, eccentricity):
    """
    Calculate mean anomaly from true anomaly and eccentricity.

    Args:
        true_anomaly (float): current true anomaly of the satellite, in radians.
        eccentricity (float): eccentricity of the satellite's orbit.

    Returns:
        mean_anomaly (float): eccentric anomaly of satellite, in radians.
    """
    beta = math.sqrt(1 - eccentricity**2) * math.sin(true_anomaly)  # shared factor
    cos_true_anomaly = math.cos(true_anomaly)

    mean_anomaly = (
        math.atan2(-beta, -eccentricity - cos_true_anomaly)
        + math.pi
        - eccentricity * (beta / (1 + eccentricity * cos_true_anomaly))
    )
    return mean_anomaly


def datetime_to_jday(epoch):
    """
    Calculate the jday representation of a UTC epoch.

    Args:
        epoch (datetime.datetime): Epoch to be converted, in UTC datetime format.

    Returns:
        jday_epoch (float): Julian day representation (as used in TLEs) of the epoch.
    """
    tt = epoch.timetuple()
    j_year_string = f"{tt.tm_year - 2000}{tt.tm_yday:0>3d}"
    j_year = float(j_year_string)
    jday_epoch = (
        j_year
        + (epoch.hour / 24)
        + (epoch.minute / (60 * 24))
        + (epoch.second / (60 * 60 * 24))
        + (epoch.microsecond / (1000000 * 60 * 60 * 24))
    )
    return jday_epoch
