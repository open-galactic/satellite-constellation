import json
from datetime import datetime
from typing import Optional, Callable, TextIO, Any

import numpy as np
import urllib.parse

from satellite_constellation.constellation import Constellation
from satellite_constellation.satellite import Satellite

from satellite_constellation.utils import (
    get_body_mass,
    constants,
    meanAnomaly,
    datetime_to_jday,
    SPACE_COCKPIT_BASE_URL,
    URL_LENGTH_LIMIT,
)


class SceneWriter:
    """Write a generated constellation to an external format.

    Attributes:
        constellation (constellation.Constellation): Satellite constellation.
        start_epoch (datetime.datetime): UTC epoch of start of mission.
        end_epoch (datetime.datetime, optional, default=None): UTC epoch of end of mission.
        filename (string, optional, default=None): Name of output file WITHOUT file extension.
        naming_function (function, optional, default=SceneWriter._name_by_index): Function used to name
            the constellation's satellites according to their index (beginning at 0).
        num_sats (int): Number of satellites in the constellation.
        mission_duration (float): Specified mission duration in seconds. If no end_epoch, defaults to 86400s
            (1 mean solar day).


    Public Methods:
        write_space_cockpit_scene() -> None: Writes the constellation to <filename>.sc in Space Cockpit's
            data format.
        write_space_cockpit_url() -> str: Get a URL to open the constellation in Space Cockpit, which is
            also written to <filename>.txt.
            NOTE: URLs are only possible for 10 or fewer satellites. Larger constellations will
            raise a ValueError.
        write_gmat_scene() -> None: Writes the constellation to <filename>.script in GMAT's data format.
        write_pigi_scene() -> None: Writes the constellation to <filename>.json in P.I.G.I.'s data format.

    Example Usage:
        Create a constellation and then write it out in each of the possible output formats:
    ```
    test_constellation = WalkerConstellation(
                num_sats=8,
                num_planes=4,
                phasing=30,
                inclination=56.2,
                altitude=650,
                beam_width=15,
            )

    writer = SceneWriter(
        constellation=test_constellation,
        utc_start_epoch=datetime.utcnow(),
    )

    # Output to P.I.G.I. scene (on disk)
    writer.write_pigi_scene()

    # Output to Space Cockpit - scene file (on disk)
    writer.write_space_cockpit_scene()

    # Output to Space Cockpit - URL (NOTE: also saved to disk)
    print(writer.write_space_cockpit_url())

    # Output to GMAT (on disk)
    writer.write_gmat_scene()

    # Output to a non-disk file-like
    with StringIO() as f:
        writer.write_space_cockpit_scene(file_like=f)
    ```
    """

    def __init__(
        self,
        constellation: Constellation,
        utc_start_epoch: datetime,
        utc_end_epoch: Optional[datetime] = None,
        filename: Optional[str] = None,
        naming_function: Optional[Callable[[int], str]] = None,
    ) -> None:
        self._scene_writer_errors(constellation, utc_start_epoch, utc_end_epoch, filename, naming_function)

        self.constellation = constellation
        self.start_epoch = utc_start_epoch
        self.end_epoch = utc_end_epoch
        self.filename = filename if filename is not None else "my_constellation"
        self.naming_function = naming_function if naming_function is not None else self._name_by_index

    @property
    def num_sats(self):
        """
        Number of satellites in the constellation
        """
        return len(self.constellation.satellites)

    @property
    def mission_duration(self):
        """
        Mission duration in seconds. If no end_epoch is specified, defaults to 86400 seconds
        (1 mean solar day).
        """
        if self.end_epoch is not None:
            mission_duration = (self.end_epoch - self.start_epoch).total_seconds()
        else:
            mission_duration = 86400  # default to 1 day
        return mission_duration

    def _scene_writer_errors(
        self,
        constellation: Constellation,
        utc_start_epoch: datetime,
        utc_end_epoch: Optional[datetime],
        filename: Optional[str],
        naming_function: Optional[Callable[[int], str]],
    ) -> None:
        """
        Check for errors with the inputs provided to SceneWriter.
        """
        if not isinstance(constellation, Constellation):
            raise TypeError("SceneWriter only accepts a Constellation as input.")

        if len(constellation.satellites) == 0:
            raise ValueError("Constellation cannot be empty.")

        if not isinstance(utc_start_epoch, datetime):
            raise TypeError("Start epoch must be a datetime.datetime instance.")

        if utc_end_epoch is not None:
            if not isinstance(utc_end_epoch, datetime):
                raise TypeError("End epoch must be a datetime.datetime instance.")
            if not utc_end_epoch > utc_start_epoch:
                raise ValueError("End epoch cannot be before start epoch.")

        if filename is not None and len(filename) == 0:
            raise ValueError("Filename cannot be empty.")

        if naming_function is not None:
            if not callable(naming_function):
                raise TypeError("Naming function must be callable.")

            generated_names = [naming_function(ind) for ind in range(len(constellation.satellites))]
            if len(generated_names) != len(set(generated_names)):
                raise ValueError("Naming function must produce a unique name for each satellite.")

    def _open_file_on_disk(self, file_ext: str) -> TextIO:
        """
        Create & open a file on disk with the already-provided filename & given file extension.
        """
        return open(f"{self.filename}{file_ext}", "w")

    def _write(self, file_like: Optional[TextIO], scene_data: str, file_ext: str) -> None:
        """
        Write the scene_data to file (or file-like object). If no file-like object is specified, the
        scene_data is written to <filename>.<file_ext> on disk.
        NOTE: Users are responsible for opening & closing of the file-like object if provided.
        """
        if file_like is None:  # handle open/close ourselves if no file_like supplied
            with self._open_file_on_disk(file_ext=file_ext) as f:
                f.write(scene_data)
        else:  # if user supplies a file_like, they must handle the open/close
            file_like.write(scene_data)

    def _name_by_index(self, index: int) -> str:
        """
        Name satellites by their index, as 'Sat_<index>', where the index is zero-padded appropriately
        for the number of satellites in the constellation (i.e. all names are the same length).
        """
        num_zeros = np.ceil(np.log10(self.num_sats) + 1).astype(int)
        return f"Sat_{index:0>{num_zeros}}"

    def _get_satellite_name_list(self) -> list[str]:
        """
        Get a list of satellite names by applying the naming_function to each satellite index.
        """
        sat_names = [self.naming_function(i) for i in range(self.num_sats)]
        return sat_names

    def _apply_TLE_checksum(self, line_str: str) -> str:
        """
        Calculate the checksum (following NORAD convention) for a single line of a TLE.
        """
        total = 0
        for char in line_str[:-1]:
            if char.isdigit():
                total += int(char)
            elif char == "-":
                total += 1
        checksum = total % 10
        tle_line = line_str[:-1] + str(checksum)
        return tle_line

    def _tle_representation(self, satellite: Satellite, epoch: datetime) -> tuple[str, str]:
        """
        Get the TLE representation of a satellite's position & orbit at the given epoch.
        """
        RADS_PER_REV = 2 * np.pi
        SECONDS_PER_DAY = 86400  # mean solar day (ref: https://celestrak.org/columns/v04n03/#FAQ03)
        (
            semimajor_axis,
            eccentricity,
            inclination,
            right_ascension,
            arg_of_periapsis,
            true_anomaly,
        ) = satellite[
            "coes"
        ]  # angles in radians

        jday_epoch = datetime_to_jday(epoch)
        clipped_eccentricity = f"{eccentricity:.7f}"[2:]

        M_earth = get_body_mass("earth")  # kg

        # NOTE: assumes satellite masses are negligible
        mean_anomaly = meanAnomaly(true_anomaly, eccentricity)
        mean_motion_rad_per_s = np.sqrt(constants["G"] * M_earth / (semimajor_axis * 1e3) ** 3)  # rad/s
        mean_motion = mean_motion_rad_per_s * SECONDS_PER_DAY / RADS_PER_REV

        tle_line_1 = self._apply_TLE_checksum(
            f"1 00000U 00000A   {jday_epoch:.8f} -.00000000  00000-0 -00000-0 0  0000"
        )
        tle_line_2 = self._apply_TLE_checksum(
            f"2 00000 {np.rad2deg(inclination): >8.4f} {np.rad2deg(right_ascension): >8.4f} "
            f"{clipped_eccentricity: >7s} {np.rad2deg(arg_of_periapsis): >8.4f}"
            f" {np.rad2deg(mean_anomaly): >8.4f} {mean_motion: >17.14f}"
        )

        return tle_line_1, tle_line_2

    def _space_cockpit_representation(self, satellite: Satellite, sat_name: str, epoch: datetime) -> dict:
        """
        Represent a single satellite in the format needed for Space Cockpit.
        """
        (
            semimajor_axis,
            eccentricity,
            inclination,
            right_ascension,
            arg_of_periapsis,
            true_anomaly,
        ) = satellite[
            "coes"
        ]  # angles in radians

        sat_data = {
            "SatNo": sat_name,
            "Nickname": "",
            "CustomName": sat_name,
            "CountryCode": "",
            "SemiMajorAxisKm": float(semimajor_axis),
            "Eccentricity": float(eccentricity),
            "Inclination": np.rad2deg(inclination),  # [deg]
            "Raan": np.rad2deg(right_ascension),  # [deg]
            "ArgPeri": np.rad2deg(arg_of_periapsis),  # [deg]
            "TrueAnomaly": np.rad2deg(true_anomaly),  # [deg]
            "Epoch": epoch.isoformat(),
            "Ephemeris": None,
            "IsCustom": True,
        }

        return sat_data

    def _gmat_representation(self, satellite: Satellite, sat_name: str, epoch: datetime) -> list[str]:
        """
        Represent a single satellite in the format needed for NASA's GMAT.
            NOTE: This representation is intentionally incomplete - GMAT will fill the rest with default
                    values automatically, this is just a minimal starting point.
        """
        (
            semimajor_axis,
            eccentricity,
            inclination,
            right_ascension,
            arg_of_periapsis,
            true_anomaly,
        ) = satellite[
            "coes"
        ]  # angles in radians

        sat_data = [
            f"Create Spacecraft {sat_name};",
            f"GMAT {sat_name}.DateFormat = UTCGregorian;",
            f"GMAT {sat_name}.Epoch = '{epoch.strftime('%d %b %Y %H:%M:%S.%f'[:-3])}';",
            f"GMAT {sat_name}.CoordinateSystem = EarthMJ2000Eq;",
            f"GMAT {sat_name}.DisplayStateType = Keplerian;",
            f"GMAT {sat_name}.SMA = {float(semimajor_axis)};",
            f"GMAT {sat_name}.ECC = {float(eccentricity)};",
            f"GMAT {sat_name}.INC = {np.rad2deg(inclination)};",
            f"GMAT {sat_name}.RAAN = {np.rad2deg(right_ascension)};",
            f"GMAT {sat_name}.AOP = {np.rad2deg(arg_of_periapsis)};",
            f"GMAT {sat_name}.TA = {np.rad2deg(true_anomaly)};",
            f"GMAT {sat_name}.OrbitColor = Red;",
            f"GMAT {sat_name}.TargetColor = Yellow;",
        ]
        return sat_data

    def _pigi_representation(self, satellite: Satellite, sat_name: str, epoch: datetime) -> dict:
        """
        Represent a single satellite in the format needed for P.I.G.I..
        """
        tle_line_1, tle_line_2 = self._tle_representation(satellite, epoch)

        sat_data = {
            "name": sat_name,
            "saberID": None,
            "noradID": "",
            "line1": tle_line_1,
            "line2": tle_line_2,
            "objectType": "PAY",
            "category": 0,
            "model": 0,
            "customModelName": None,
            "planet": 3,  # Earth
            "favourite": False,
            "groundFOV": 180.0,
            "spaceFOV": 180.0,
            "showGroundFOV": False,
            "showSpaceFOV": False,
            "hideOrbitLine": False,
            "hideName": False,
        }
        return sat_data

    def write_space_cockpit_scene(self, file_like: Optional[TextIO] = None) -> None:
        """
        Create a Space Cockpit scene & write it to <filename>.sc  (default) or a provided file-like object.
        """
        sat_list = self.constellation.satellites
        sat_names = self._get_satellite_name_list()

        # fill out the data Space Cockpit needs
        orbit_visuals = [
            {"ToolType": "Orbit", "InvolvedSceneObjectIds": [sat_name]} for sat_name in sat_names
        ]
        scene_data = {
            "Id": "",
            "Name": "Constellation scene",
            "ActiveAssetListIndex": 0,
            "AssetLists": [
                {
                    "Name": "My Constellation",
                    "Sats": sat_names,
                    "HiddenSatIds": [],
                    "Sensors": [],
                    "HiddenSensorIds": [],
                }
            ],
            "UserSatData": [
                self._space_cockpit_representation(sat, sat_name, self.start_epoch)
                for sat, sat_name in zip(sat_list, sat_names)
            ],
            "UserSensorData": [],
            "FocusedSat": None,
            "FocusedSensor": None,
            "Visuals": [
                *orbit_visuals,
                {"ToolType": "GeoBeltOrbit", "InvolvedSceneObjectIds": []},
                {"ToolType": "GeoBeltMarker", "InvolvedSceneObjectIds": []},
                {"ToolType": "GeoBeltMarker", "InvolvedSceneObjectIds": []},
                {"ToolType": "GeoBeltMarker", "InvolvedSceneObjectIds": []},
                {"ToolType": "GeoBeltMarker", "InvolvedSceneObjectIds": []},
                {"ToolType": "GeoBeltMarker", "InvolvedSceneObjectIds": []},
                {"ToolType": "GeoBeltMarker", "InvolvedSceneObjectIds": []},
                {"ToolType": "GeoBeltLonHover", "InvolvedSceneObjectIds": [None]},
            ],
            "SimulationTime": self.start_epoch.isoformat(),
            "SimulationSpeed": 1.0,
            "ScenarioEndTime": self.end_epoch.isoformat() if self.end_epoch is not None else None,
            "Status": None,
        }

        self._write(scene_data=json.dumps(scene_data, indent=2), file_like=file_like, file_ext=".sc")

    def write_space_cockpit_url(self, file_like: Optional[TextIO] = None) -> str:
        """
        Create a Space Cockpit URL representing the scene, saved in <filename>.txt (default) or a provided
        file-like object.
        NOTE: A ValueError is raised if the URL exceeds length limitations (typically > 10 satellites).
        """
        constellation_tle_spec = {}

        for sat_ind, satellite in enumerate(self.constellation.satellites):
            tle_identifier = f"TLE{sat_ind}"
            tle_line_1, tle_line_2 = self._tle_representation(satellite, self.start_epoch)
            constellation_tle_spec[tle_identifier] = f"{tle_line_1}{tle_line_2}"

        encoded_tles = urllib.parse.urlencode(constellation_tle_spec, quote_via=urllib.parse.quote)
        link_url = f"{SPACE_COCKPIT_BASE_URL}?{encoded_tles}"

        url_length = len(link_url)
        if url_length > URL_LENGTH_LIMIT:
            raise ValueError(
                "Constellation is too large to encode as a single URL. "
                "Please reduce the number of satellites or download the data as a scene file instead."
            )

        self._write(scene_data=link_url, file_like=file_like, file_ext=".txt")
        return link_url

    def write_gmat_scene(self, file_like: Optional[TextIO] = None) -> None:
        """
        Create a GMAT scene & write it to <filename>.script  (default) or a provided file-like object.
        NOTE: Only minimal specs are provided, as GMAT will fill in the rest with default values.
        """
        sat_list = self.constellation.satellites
        sat_names = self._get_satellite_name_list()

        scene_data: dict[str, list[Any]] = {  # <section header>: <section content>
            "Metadata": [],
            "Spacecraft": [],
            "Force Models": [],
            "Propagator": [],
            "Subscribers": [],
            "Mission": [],
        }

        # metadata
        scene_data["Metadata"].extend(
            [f"%Created by Satellite-Constellation creator at {datetime.utcnow().isoformat()}"]
        )

        # create spacecraft
        for sat, sat_name in zip(sat_list, sat_names):
            scene_data["Spacecraft"].extend(self._gmat_representation(sat, sat_name, self.start_epoch))
            scene_data["Spacecraft"].append("")  # add spacing between spacecraft for readability

        # set up force models
        scene_data["Force Models"].extend(
            [
                "Create ForceModel DefaultProp_ForceModel;",
                "GMAT DefaultProp_ForceModel.CentralBody = Earth;",
                "GMAT DefaultProp_ForceModel.PrimaryBodies = {Earth};",
            ]
        )

        # set up propagator
        scene_data["Propagator"].extend(["Create Propagator DefaultProp;"])

        # set up subscribers (results)
        comma_sat_list = ", ".join(sat_names)
        scene_data["Subscribers"].extend(
            [
                "Create OrbitView DefaultOrbitView;",
                "GMAT DefaultOrbitView.SolverIterations = Current;",
                f"GMAT DefaultOrbitView.Add = {{Earth, {comma_sat_list}}};",
                "GMAT DefaultOrbitView.CoordinateSystem = EarthMJ2000Eq;",
                f"GMAT DefaultOrbitView.DrawObject = [{' '.join('true' for _ in range(self.num_sats))}];",
                "GMAT DefaultOrbitView.ViewPointReference = Earth;",
                "GMAT DefaultOrbitView.ViewPointVector = [ 30000 0 0 ];",
                "GMAT DefaultOrbitView.ViewDirection = Earth;",
                "GMAT DefaultOrbitView.ViewScaleFactor = 1;",
                "GMAT DefaultOrbitView.ViewUpCoordinateSystem = EarthMJ2000Eq;",
                "GMAT DefaultOrbitView.ViewUpAxis = Z;",
                "GMAT DefaultOrbitView.EclipticPlane = Off;",
                "GMAT DefaultOrbitView.XYPlane = On;",
                "",
                "Create GroundTrackPlot DefaultGroundTrackPlot;",
                "GMAT DefaultGroundTrackPlot.SolverIterations = Current;",
                f"GMAT DefaultGroundTrackPlot.Add = {{{comma_sat_list}}};",
            ]
        )

        # set up mission
        scene_data["Mission"].extend(
            [
                "BeginMissionSequence;",
                f"Propagate DefaultProp({comma_sat_list}) "
                f"{{{sat_names[0]}.ElapsedSecs = {self.mission_duration}}};",
            ]
        )

        gmat_file_data = []
        for section_header, section_lines in scene_data.items():
            header_lines = [
                "%----------------------------------------",
                f"%---------- {section_header}",
                "%----------------------------------------",
            ]
            gmat_file_data.extend(header_lines)
            gmat_file_data.extend(section_lines)
            gmat_file_data.append("\n")

        self._write(scene_data="\n".join(gmat_file_data), file_like=file_like, file_ext=".script")

    def write_pigi_scene(self, file_like: Optional[TextIO] = None) -> None:
        """
        Create a P.I.G.I scene & write it to <filename>.json (default) or a provided file-like object.
        """
        sat_list = self.constellation.satellites
        sat_names = self._get_satellite_name_list()

        scene_data: dict[str, Any] = {
            "version": None,
            "satellites": [
                self._pigi_representation(sat, sat_name, self.start_epoch)
                for sat, sat_name in zip(sat_list, sat_names)
            ],
            "groundStations": [],
        }

        self._write(scene_data=json.dumps(scene_data, indent=2), file_like=file_like, file_ext=".json")
