class ConstellationNumberError(Exception):
    pass


class ConstellationConfigurationError(Exception):
    pass


class ConstellationPlaneMismatchError(Exception):
    pass


class PhaseError(Exception):
    pass


class AltitudeError(Exception):
    pass


class EccentricityError(Exception):
    pass


class BeamError(Exception):
    pass


class FocusError(Exception):
    pass


class MaxSatellitesExceededError(Exception):
    pass


class StreetWidthError(Exception):
    pass


class RevisitError(Exception):
    pass
