import unittest

import numpy as np
import numpy.testing as testing

from satellite_constellation.constellation import (
    FlowerConstellation,
    SOCConstellation,
    WalkerConstellation,
)
from satellite_constellation.constellation_errors import (
    ConstellationConfigurationError,
    ConstellationNumberError,
    ConstellationPlaneMismatchError,
    FocusError,
    MaxSatellitesExceededError,
    PhaseError,
    RevisitError,
    StreetWidthError,
)
from satellite_constellation.satellite import newSatellite


class TestSatellite(unittest.TestCase):
    def setUp(self):
        self.satellite = newSatellite(1000, 0, 10, 20, 30, 40, rads=False)
        self.satellite2 = newSatellite(1000, 0.8, 23, 25, 800, 20, rads=False)

    def test_true_altitude(self):
        self.assertEqual(self.satellite["true_alt"], 7371)

    def test_deg_to_rad(self):
        D2R_error = 4
        self.assertAlmostEqual(0.1745, self.satellite["inclination"], D2R_error)
        self.assertAlmostEqual(0.3491, self.satellite["right_ascension"], D2R_error)
        self.assertAlmostEqual(0.5236, self.satellite["aop"], D2R_error)
        self.assertAlmostEqual(0.6981, self.satellite["true_anomaly"], D2R_error)

    def test_output_shape(self):
        for sat in [self.satellite, self.satellite2]:
            # state vector shape
            self.assertEqual(2, len(sat["state_vectors"]))
            position_vector, velocity_vector = sat["state_vectors"]
            self.assertEqual(3, len(position_vector))
            self.assertEqual(3, len(velocity_vector))

            # coe shape
            self.assertEqual(6, len(sat["coes"]))

    def test_coe_conversion(self):
        coe_example = [7371.0, 0, 0.1745329251994, 0.349065850398, 0.523598775598, 0.698131]

        testing.assert_almost_equal(coe_example, self.satellite["coes"], 4)

    def test_state_vector_conversion_predefined(self):
        sv_example = [
            [3710.1188410, 6315.8335413, 822.74289015],
            [-6.3392169758, 3.59642745196, 0.97820500071],
        ]
        testing.assert_almost_equal(sv_example, self.satellite["state_vectors"], 4)

    def test_state_vector_conversion_extensive(self):
        """
        checking specific angular momentum calculated from coes matches
        cross product of state vectors - only occurs if coe -> sv conversion was correct
        """
        MU = 398600.4415

        test_sats = [
            self.satellite,
            self.satellite2,
            newSatellite(3000, 0.5, 70, 50, 2800, 20),
            newSatellite(200, 0.8, 70, 50, 100, 20),
            newSatellite(300, 0.9, 30, 50, 200, 50),
            newSatellite(400, 0.6, 0, 0, 200, 2),
            newSatellite(120, 0.1, 1, 1, 80, 2),
        ]

        for sat in test_sats:
            # specific angular momentum from coe
            a, e = sat["semi_major"], sat["eccentricity"]
            specific_angular_momentum_coe = float(np.sqrt(MU * a * (1 - e**2)))

            # specific angular momentum from state vector
            r, v = sat["state_vectors"]
            r_np, v_np = np.array(r), np.array(v)

            # cross product of position and velocity vectors = spec. angular momentum
            specific_angular_momentum_sv = float(np.linalg.norm(np.cross(r_np, v_np)))

            self.assertAlmostEqual(specific_angular_momentum_coe, specific_angular_momentum_sv, 8)


class TestWalkerErrors(unittest.TestCase):
    def test_plane_mismatch(self):
        T, P, F = 1, 2, 1
        with self.assertRaises(ConstellationPlaneMismatchError):
            WalkerConstellation(T, P, F, 30, 1000)

    def test_phase_error_1(self):
        T, P, F = 1, 1, -1
        with self.assertRaises(PhaseError):
            WalkerConstellation(T, P, F, 30, 1000)

    def test_phase_error_2(self):
        T, P = 1, 1
        F: float = 1.5
        with self.assertRaises(PhaseError):
            WalkerConstellation(T, P, F, 30, 1000)

    def test_phase_error_3(self):
        T, P, F = 1, 1, 1
        with self.assertRaises(PhaseError):
            WalkerConstellation(T, P, F, 30, 1000)

    def test_supported_body(self):
        with self.assertRaises(FocusError):
            WalkerConstellation(18, 3, 1, 30, 1000, focus="The Moon")


class TestWalker(unittest.TestCase):
    def setUp(self):
        self.walker_constellation = WalkerConstellation(18, 3, 1, 30, 1000, beam_width=20)

    def test_sats_per_plane(self):
        self.assertEqual(6, self.walker_constellation.sats_per_plane)

    def test_phasing(self):
        self.assertEqual(20, self.walker_constellation.correct_phasing)

    def test_period(self):
        self.assertAlmostEqual(6298.162787579335, self.walker_constellation.orbital_period, 8)

    def test_min_revisit(self):
        self.assertEqual(28800, self.walker_constellation.minimum_revisit)

    def test_coverage_area(self):
        self.assertAlmostEqual(1767254.11319, self.walker_constellation.coverage_area, 4)

    def test_true_anomalies(self):
        expected_anomalies = [
            0.0,
            60.0,
            120.0,
            180.0,
            240.0,
            300.0,
            20.0,
            80.0,
            140.0,
            200.0,
            260.0,
            320.0,
            40.0,
            100.0,
            160.0,
            220.0,
            280.0,
            340.0,
        ]
        self.assertSequenceEqual(self.walker_constellation.ta, expected_anomalies)


class TestStreetsErrors(unittest.TestCase):
    def test_plane_mismatch(self):
        with self.assertRaises(ConstellationConfigurationError):
            self.streets_constellation = SOCConstellation(1, 5, 15000, 60, [0, 10], 0, 100)

        with self.assertRaises(ConstellationConfigurationError):
            self.streets_constellation = SOCConstellation(2, 5, 15000, 60, [0], 0, 100)

    def test_invalid_streets(self):
        with self.assertRaises(ConstellationConfigurationError):
            self.streets_constellation = SOCConstellation(0.1, 5, 15000, 60, [], 0, 100)

        with self.assertRaises(ConstellationNumberError):
            self.streets_constellation = SOCConstellation(0, 5, 15000, 60, [], 0, 100)

    def test_street_width(self):
        with self.assertRaises(StreetWidthError):
            self.streets_constellation = SOCConstellation(1, 190, 15000, 60, [0], 0, -10)

        with self.assertRaises(StreetWidthError):
            self.streets_constellation = SOCConstellation(1, -10, 15000, 60, [0], 0, -10)

    def test_street_width_access_area(self):
        with self.assertRaises(StreetWidthError):
            self.streets_constellation = SOCConstellation(1, 10, 1500, 60, [20], 0, 7770)

        with self.assertRaises(StreetWidthError):
            self.streets_constellation = SOCConstellation(3, 10, 400, 50, [0, 1, 2], 0, 4000)

    def test_revisit_time(self):
        with self.assertRaises(RevisitError):
            self.streets_constellation = SOCConstellation(1, 5, 15000, 60, [0], 0, -10)

    def test_supported_body(self):
        with self.assertRaises(FocusError):
            SOCConstellation(1, 5, 15000, 60, [0], 0, 120, focus="The Moon")


class TestStreets(unittest.TestCase):
    def setUp(self):
        self.streets_constellation = SOCConstellation(1, 5, 15000, 60, [0], 0, 100)
        self.streets_constellation_2 = SOCConstellation(1, 10, 1500, 70, [20], 0, 7700)

    def test_perigee(self):
        self.assertAlmostEqual(self.streets_constellation.perigee, 21371, 3)
        self.assertAlmostEqual(self.streets_constellation_2.perigee, 7871, 3)

    def test_semi_major(self):
        self.assertAlmostEqual(self.streets_constellation.semi_major, 21371.0, 3)
        self.assertAlmostEqual(self.streets_constellation_2.semi_major, 7871, 3)

    def test_orbital_period(self):
        self.assertAlmostEqual(self.streets_constellation.orbital_period, 31092.92, 1)
        self.assertAlmostEqual(self.streets_constellation_2.orbital_period, 6949.74921865, 3)

    def test_earth_radial_coverage(self):
        self.assertAlmostEqual(self.streets_constellation.earth_coverage_radius, 8078.94, 1)

    def test_earth_angular_coverage(self):
        self.assertAlmostEqual(self.streets_constellation.earth_coverage_angle, 1.268, 2)

    def test_required_satellites_by_coverage(self):
        self.assertAlmostEqual(self.streets_constellation.num_satellites, 5)

    def test_required_satellites_by_period(self):
        self.assertAlmostEqual(self.streets_constellation_2.num_satellites, 1, 1)

    def test_angular_spacing(self):
        self.assertAlmostEqual(self.streets_constellation.angular_spacing, 1.2668866294453092, 10)

    def test_true_spacing(self):
        self.assertAlmostEqual(self.streets_constellation.true_spacing, 1.2566370614359172, 10)
        self.assertAlmostEqual(self.streets_constellation_2.true_spacing, 6.283185307179586, 10)

    def test_sats_per_street(self):
        self.assertEqual(self.streets_constellation.sats_per_street, 5)
        self.assertEqual(self.streets_constellation_2.sats_per_street, 1)

    def test_perigee_positions(self):
        positions = [0.0, 72.0, 144.0, 216.0, 288.0]
        self.assertSequenceEqual(self.streets_constellation.perigee_positions, positions)


class TestFlowerErrors(unittest.TestCase):
    def test_max_satellites(self):
        with self.assertRaises(MaxSatellitesExceededError):
            FlowerConstellation(8, 1, 20, 1, 9, 0, 0, 2500, 20)

    def test_num_petals(self):
        with self.assertRaises(ConstellationNumberError):
            FlowerConstellation(0.8, 1, 9, 1, 9, 0, 0, 2500, 20)

    def test_num_days(self):
        with self.assertRaises(ConstellationNumberError):
            FlowerConstellation(8, 1.5, 9, 1, 9, 0, 0, 2500, 20)

    def test_num_satellites(self):
        with self.assertRaises(ConstellationNumberError):
            FlowerConstellation(8, 1, 5.5, 1, 9, 0, 0, 2500, 20)

    def test_phasing_n(self):
        with self.assertRaises(ConstellationNumberError):
            FlowerConstellation(8, 1, 9, 1.5, 9, 0, 0, 2500, 20)

    def test_num_phasing(self):
        with self.assertRaises(ConstellationNumberError):
            FlowerConstellation(8, 1, 9, 1, 9.5, 0, 0, 2500, 20)

    def test_supported_body(self):
        with self.assertRaises(FocusError):
            FlowerConstellation(769, 257, 4, 1, 4, 0, 0, 600, 20, focus="The Moon")


class TestFlower(unittest.TestCase):
    # Test cases from "The Flower Constellations - Theory, Design Process and Applications" - Wilkins, P.M
    def setUp(self):
        self.flower_suite = [
            FlowerConstellation(8, 1, 9, 1, 9, 0, 0, 2500, 20),
            FlowerConstellation(769, 257, 4, 1, 4, 0, 0, 600, 20),
            FlowerConstellation(4, 1, 4, 1, 4, 0, 0, 600, 20),
            FlowerConstellation(3, 1, 4, 1, 4, 0, 0, 600, 20),
            FlowerConstellation(3, 1, 4, 1, 7, 0, 0, 600, 20),
            FlowerConstellation(3, 2, 4, 1, 2, 0, 0, 600, 20),
            FlowerConstellation(31, 11, 30, 7, 10, 0, 0, 9000, 20),
            FlowerConstellation(37, 18, 57, 6, 19, 0, 0, 19702, 20),
            FlowerConstellation(15, 7, 49, 23, 49, 0, 0, 19702, 20),
        ]

        self.aop_suite = [
            FlowerConstellation(3, 2, 4, 1, 2, 0, 0, 600, 20),
            FlowerConstellation(31, 11, 30, 7, 10, 30, 0, 9000, 20),
            FlowerConstellation(37, 18, 57, 6, 19, 60, 0, 19702, 20),
            FlowerConstellation(15, 7, 49, 23, 49, 130, 0, 19702, 20),
            FlowerConstellation(4, 1, 4, 1, 4, 0, 300, 600, 20),
        ]

    def test_num_satellites(self):
        num_sats = []
        num_sats_test = [9, 4, 4, 4, 4, 4, 30, 57, 49]
        for idx in range(len(self.flower_suite)):
            num_sats.append(self.flower_suite[idx].num_satellites)
        self.assertEqual(num_sats, num_sats_test)

    def test_max_satellites(self):
        max_sats = []
        max_sats_test = [9, 1028, 4, 4, 7, 4, 110, 342, 343]
        for idx in range(len(self.flower_suite)):
            max_sats.append(self.flower_suite[idx].max_sats)
        self.assertEqual(max_sats, max_sats_test)

    def test_revisit_time(self):
        revis_times = [0.1111, 64.25, 0.25, 0.25, 0.25, 0.5, 0.3667, 0.3158, 0.1429]

        testing.assert_almost_equal(revis_times, [const.revisit_time for const in self.flower_suite], 4)

    def test_minimum_revisit_time(self):
        min_revis_times = [0.1111, 0.25, 0.25, 0.25, 0.1429, 0.5, 0.1, 0.0526, 0.0204]

        testing.assert_almost_equal(
            min_revis_times, [const.minimum_revisit_time for const in self.flower_suite], 4
        )

    def test_max_sats_per_orbit(self):
        max_sats_per_orbit = []
        max_sats_per_orbit_test = [1, 257, 1, 1, 1, 2, 11, 18, 7]
        for idx in range(len(self.flower_suite)):
            max_sats_per_orbit.append(self.flower_suite[idx].max_sats_per_orbit)
        self.assertEqual(max_sats_per_orbit, max_sats_per_orbit_test)

    def test_raan_spacing(self):
        raan_spacing_test = [-40, -90, -90, -90, -51.42, -180, -252, -113.68, -168.97]
        for idx in range(len(self.flower_suite)):
            self.assertAlmostEqual(raan_spacing_test[idx], self.flower_suite[idx].raan_spacing, delta=0.1)

    def test_mean_anomaly_spacing(self):
        mean_anomaly_spacing_test = [320, 269.2, 360, 270, 154.28, 270, 350.18, 233.68, 2.099]
        for idx in range(len(self.flower_suite)):
            self.assertAlmostEqual(
                mean_anomaly_spacing_test[idx], self.flower_suite[idx].mean_anomaly_spacing, delta=0.1
            )

    def test_raan_anomaly_simple(self):
        param_list = []
        param_list_test = [
            [0, 0],
            [40, 40],
            [80, 80],
            [120, 120],
            [160, 160],
            [200, 200],
            [240, 240],
            [280, 280],
            [320, 320],
        ]
        for idx in range(len(self.flower_suite[0].raan)):
            param_list.append([self.flower_suite[0].raan[idx], self.flower_suite[0].mean_anomaly[idx]])

        self.assertTrue(len(param_list) == len(param_list_test))

        for idx in range(len(param_list)):
            self.assertTrue(param_list_test[idx] in param_list)

    def test_raan_anomaly_complex(self):
        param_list = []
        param_list_test = [[0, 0], [0, 180], [180, 90], [180, 270]]
        for idx in range(len(self.flower_suite[5].raan)):
            param_list.append([self.flower_suite[5].raan[idx], self.flower_suite[5].mean_anomaly[idx]])

        for idx in range(len(param_list)):
            self.assertTrue(param_list_test[idx] in param_list)

    def test_aop_new_constellations(self):
        expected_aops = [0, 30, 60, 130, 300, 20]

        self.assertTrue(
            [constellation.perigee_argument == aop]
            for constellation, aop in zip(self.aop_suite, expected_aops)
        )

    def test_aop_existing_constellations(self):
        existing_aop = 0.0

        self.assertTrue(
            [constellation.perigee_argument == existing_aop for constellation in self.flower_suite]
        )


if __name__ == "__main__":
    unittest.main()
