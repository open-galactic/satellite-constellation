import math
from typing import List, Tuple, Union

import numpy as np

from satellite_constellation.constellation_errors import (
    StreetWidthError,
    constellation_errors,
    flower_errors,
    street_errors,
    walker_errors,
)
from satellite_constellation.satellite import Satellite, newSatellite
from satellite_constellation.utils import (
    constants,
    eccentricAnomaly,
    get_body_mass,
    get_body_period,
    get_body_radius,
    normalise_angle,
    to_positive_angle,
    trueAnomaly,
)


class Constellation:
    """
    Class to implement general constellation features and behaviour
    Contains parameters general to all types of satellites

    Args:
        num_satellites: Number of satellites used in the constellation
        orbital_period: Orbital period of the satellites [s]
        altitude: altitude of the satellites [km]
        beam_width: Sensor beam width of the satellites [degrees]
        eccentricity: Eccentricity of satellite orbits
        inclination: Inclination of orbit relative to equatorial plane "i" [degrees]
        focus: Heavenly body at the focus of the orbit, defaults to 'Earth'
    """

    def __init__(
        self,
        num_sats,
        altitude,
        beam_width,
        eccentricity,
        inclination,
        focus="earth",
    ):
        constellation_errors(altitude, beam_width, eccentricity, focus)

        # informaion about the orbited body
        self.body_radius = get_body_radius(focus)
        self.body_mass = get_body_mass(focus)
        self.body_period = get_body_period(focus)

        # constellation information
        self.num_satellites = num_sats
        self.altitude = altitude
        self.beam_width = beam_width
        self.eccentricity = eccentricity
        self.inclination = inclination
        self.focus = focus
        self.satellites = []
        self.earth_coverage_radius = 0
        self.orbital_period = self.__calculate_orbital_period()

    def __calculate_orbital_period(self) -> float:
        """
        Calculate orbital period from semi-major axis, eccentricity and perigee altitude
        """

        perigee = self.body_radius + self.altitude  # [km]
        self.semi_major = perigee / (1 - self.eccentricity)  # [km]
        orbital_period = (
            2 * math.pi * math.sqrt((self.semi_major * 10**3) ** 3 / (self.body_mass * constants["G"]))
        )  # [s]
        return orbital_period

    def as_state_vectors(self) -> list:
        """
        Returns the state vectors of each satellite in the constellation.

        Returns:
            vectors: list of satellites, each presented as a list of state vector lists
        """
        vectors = [satellite["state_vectors"] for satellite in self.satellites]

        return vectors

    def as_coes(self) -> list:
        """
        Returns the coes of each satellite in the constellation

        Returns:
            coes: list of satllite, each represented as a list of coes
        """

        coes = [satellite["coes"] for satellite in self.satellites]

        return coes


class WalkerConstellation(Constellation):
    """
    Class for describing and holding a walker constellation of satellites

    Args:
        num_sats (int): Number of satellites used in the constellation "t"
        num_planes (int): The number of different orbit planes in the constellation "p"
        phasing (int): Dictates the spacing between equivalent satellites in neighbouring orbital planes "f"
        inclination (float): Inclination of orbit relative to equatorial plane "i" [degrees]
        altitude (float): Altitude of satellites in orbit [km]
        eccentricity (float): Eccentricity of satellite orbits
        beam_width (float): Sensor beam width of the satellites [degrees]
        focus (string): Heavenly body at the focus of the orbit, defaults to 'Earth'
    """

    def __init__(
        self,
        num_sats,
        num_planes,
        phasing,
        inclination,
        altitude,
        beam_width=1,
        raan_0=0,
        focus="earth",
        starting_number=0,
    ):
        super(WalkerConstellation, self).__init__(num_sats, altitude, beam_width, 0, inclination, focus)

        walker_errors(num_sats, num_planes, phasing)

        self.plane_range = 360
        self.perigee_0 = 0.0
        self.num_planes = num_planes
        self.phasing = phasing
        self.start_num = starting_number
        self.sats_per_plane, self.correct_phasing = self.__corrected_planes()
        self.raan_0 = raan_0
        self.perigee_positions = self.__perigee_positions()
        self.raan = self.__calculate_raan()
        self.ta = self.__calculate_true_anom()
        self.satellites = self.__build_satellites()

        # earth coverage radius, earth coverage angle, coverage area
        e_c_r, e_c_a, c_a = self.__calculate_simple_coverage()
        self.earth_coverage_radius = e_c_r
        self.earth_coverage_angle = e_c_a
        self.coverage_area = c_a

        self.minimum_revisit = self.__calculate_minimum_revisit()

    def __corrected_planes(self) -> Tuple[int, float]:
        """
        Calculate the allowed number of satellites per plane, and the adjusted phasing to accomodate
        this number of satellites, based on the required num. of satellites, available planes and
        requested parameter
        """

        # truncate the number of satellites per plane to get whole number
        sats_per_plane = int(self.num_satellites / self.num_planes)
        corrected_phasing = self.plane_range * self.phasing / self.num_satellites

        return sats_per_plane, corrected_phasing

    def __perigee_positions(self) -> List[float]:
        """
        Argument of Perigees are shared between satellites in a Walker Delta, set to base value
        """

        all_perigees = [self.perigee_0 for _ in range(self.num_satellites)]

        return all_perigees

    def __calculate_raan(self) -> List[int]:
        """
        Calculate right ascension for each satellite across all planes.

        RAAN of neighbouring satellites in a plane should be equal, while RAAN of satellites in
        neighbouring planes should be seperated by the spacing paramter.
        """
        raan = [0] * self.num_satellites
        plane_spacing = self.plane_range / self.num_planes

        for i in range(self.sats_per_plane, self.num_satellites):
            # raan for some plane = raan of previous plane + spacing parameter
            raan[i] = self.raan_0 + raan[i - self.sats_per_plane] + plane_spacing

        return raan

    def __calculate_true_anom(self) -> List[float]:
        """
        Calculate true anomaly for each satellite across all planes

        True anom of neighbouring satellites in a plane should be the same, while satellites in
        neighbouring planes should be seperated by the corrected phasing param
        """

        # generate the equally spaced anomalies for a single orbital plane
        satellite_spacing = 360 / self.sats_per_plane
        initial_anomalies = [sat_index * satellite_spacing for sat_index in range(self.sats_per_plane)]

        # add anomalies for each plane to the total list
        true_anomalies = []
        for plane_index in range(self.num_planes):
            # offset each plane by the phase value for that plane - delta phi / self.correct_phasing
            plane_phasing = plane_index * self.correct_phasing
            final_anomalies = [normalise_angle(init_anom + plane_phasing) for init_anom in initial_anomalies]

            # add plane to true anomalies
            true_anomalies.extend(final_anomalies)

        return true_anomalies

    def __calculate_simple_coverage(self) -> Tuple[int, float, float]:
        """
        Function to calculate the coverage radius and angle of satellite on the orbited bodies surface
        """

        half_width = np.radians(self.beam_width / 2)
        r = self.altitude + self.body_radius
        max_width = math.asin(self.body_radius / r)
        if half_width > max_width:
            half_width = max_width

        theta = math.asin(math.sin(half_width) / (self.body_radius / r)) - half_width
        r = self.body_radius * theta
        area = math.pi * math.pow(r, 2)
        total_area = area * self.num_satellites

        return r, theta, total_area

    def __calculate_minimum_revisit(self) -> float:
        """
        Calculates a rough estimate of the minimum possible revisit time for the constellation, acting
        as a lower bound.
        Does not account for beam width.
        """

        return self.body_period * 24 * 60 * 60 / self.num_planes

    def __build_satellites(self) -> List[Satellite]:
        """
        Creates a list of satellite objects based on the orbital parameters determined on initialisation
        """

        satellites = [
            newSatellite(
                self.altitude,
                self.eccentricity,
                self.inclination,
                self.raan[index],
                self.perigee_positions[index],
                self.ta[index],
                focus=self.focus,
                rads=False,
            )
            for index in range(self.num_satellites)
        ]

        return satellites


class SOCConstellation(Constellation):

    """
    Class for describing and holding a walker constellation of satellites

    Args:
        num_streets (PositiveInt): Number of streets used in the constellation
        street_width (float): Angular width of street [degrees]
        altitude (float): Altitude of satellites in orbit [km]
        beam_width (float): Sensor beam width of the satellites [degrees]
        raan (list[NonNegativeFloat]): List of right ascension for each street [degrees]
        eccentricity (float): Eccentricity of satellite orbits
        revisit_time (PositiveFloat): Time until latitude re-enters satellite coverage [s]
        focus (string): Heavenly body at the focus of the orbit, defaults to 'Earth'
    """

    def __init__(
        self,
        num_streets,
        street_width,
        altitude,
        beam_width,
        raan,
        eccentricity,
        revisit_time,
        focus="earth",
        starting_number=0,
    ):
        super(SOCConstellation, self).__init__(0, altitude, beam_width, eccentricity, 90, focus)

        street_errors(num_streets, street_width, raan, revisit_time)

        self.num_streets = num_streets
        self.start_num = starting_number
        self.raan = raan
        self.street_width = street_width
        self.revisit_time = revisit_time
        self.earth_coverage_radius, self.earth_coverage_angle = self.__calculate_earth_coverage()
        self.linear_spacing, self.angular_spacing = self.__calculate_spacing()
        self.perigee, self.semi_major, self.orbital_period = self.__calculate_orbit_params()
        self.num_satellites, self.sats_per_street, self.true_spacing = self.__calculate_required_satellites()
        self.perigee_positions = self.__perigee_positions()
        self.ta = self.__calculate_true_anom()
        self.satellites = self.__build_satellites()

    def __calculate_earth_coverage(self) -> Tuple[int, float]:
        """

        Determines the coverage radius and angle of a satellite on the surface of the orbited body

        """

        half_width = np.radians(self.beam_width / 2)
        r = self.altitude + self.body_radius
        max_width = math.asin(self.body_radius / r)
        half_width = min(half_width, max_width)

        theta = math.asin(math.sin(half_width) / (self.body_radius / r)) - half_width
        r = self.body_radius * theta

        return r, theta

    def __calculate_spacing(self) -> Tuple[float, float]:
        """
        Determines the spacing of satellites in the orbit to create a street of coverage of given width
        """

        # check street width isn't larger than the access area
        street_width_arc = self.body_radius * np.radians(self.street_width)
        if street_width_arc > self.earth_coverage_radius:
            raise StreetWidthError("Street width larger than the maximum width of coverage / access area")

        lambda_street = np.radians(self.street_width)  # [rads]
        lambda_max = self.earth_coverage_angle  # [rads]

        # Refer to Space Mission Engineering: The New SMAD (ISBN 978-1-881-883-15-9) for more info (eqn 10-23)
        # (S)pacing / 2 = arccos(cos(lambda_max) / cos(lambda_street))
        lambda_max_cos = math.cos(lambda_max)
        lambda_street_cos = math.cos(lambda_street)

        # calculate angular and linear spacing
        half_spacing = math.acos(lambda_max_cos / lambda_street_cos)
        linear_spacing = half_spacing / self.body_radius

        return linear_spacing, half_spacing

    def __calculate_orbit_params(self) -> Tuple[float, float, float]:
        """
        Determines the perigee altitude, semi major axis and orbital period of the orbits
        """

        perigee = self.body_radius + self.altitude  # [km]
        semi_major = perigee / (1 - self.eccentricity)  # [km]
        mu = self.body_mass * constants["G"]  # standard gravitational parameter

        # calculating orb. period from semi major axis, by Kepler's Third Law
        orbital_period = 2 * math.pi * math.sqrt((semi_major * 10**3) ** 3 / mu)  # [seconds]

        return perigee, semi_major, orbital_period

    def __calculate_required_satellites(self) -> Tuple[float, int, float]:
        """
        Determines the number of satellites required to cover a street of given width.
        """

        # find lowest requirement on number of satellites: limited by orbital period vs. by spacing
        period_limit = self.orbital_period / self.revisit_time  # Calculated from revisit time
        spacing_limit = 2 * math.pi / self.angular_spacing  # Total coverage

        num_satellites = min(period_limit, spacing_limit)

        # num_satellites unlikely to be whole number, so we need upper and lower
        # bounds on how many satellites we want.
        upper = math.ceil(num_satellites)
        lower = math.floor(num_satellites)

        # if lower bound is only 0.01 less than required number, we can round down, else up
        # (operates under the assumption that 0.01 is an acceptable tolerance)
        bound = lower if (num_satellites - lower <= 0.01) else upper

        # calculate satellite spacing parameters based on selected bound and return
        true_spacing = 2 * math.pi / bound
        total_satellites = bound * self.num_streets
        sats_per_street = bound

        return total_satellites, sats_per_street, true_spacing

    def __calculate_true_anom(self) -> List[float]:
        """
        Calculate true anomaly for each satellite across all planes

        True anom of neighbouring satellites in a plane should be the same, while satellites in
        neighbouring planes should be seperated by the corrected phasing param
        """

        self.correct_phasing = 360 / self.num_satellites
        ta: List[float] = [0] * self.num_satellites
        for i in range(self.sats_per_street, self.num_satellites):
            ta[i] = ta[i - self.sats_per_street] + self.correct_phasing

        return ta

    def __perigee_positions(self) -> List[float]:
        """
        Generate angular positions of perigees for 'sats_per_street' equally spaced satellites
        across 'num_streets' streets.
        """

        # limit maximum angle to prevent overlapped satellites (one at 0 deg, the other at 360 deg)
        ang_lim = 360 - (360 / self.sats_per_street)
        perigees = np.linspace(0, ang_lim, self.sats_per_street)

        # perigee offsets will be the same for all 'num_streets' planes
        all_perigees: List[float] = []
        for _ in range(self.num_streets):
            all_perigees.extend(perigees)

        return all_perigees

    def __build_satellites(self) -> List[Satellite]:
        """
        Creates a list of satellite objects based on the orbital parameters determined on initialisation
        """

        satellites: list = []
        positions = np.array(self.perigee_positions) + np.array(self.ta)

        for street in range(self.num_streets):
            for sat in range(self.sats_per_street):
                index = sat + (street * self.sats_per_street)
                satellites.append(
                    newSatellite(
                        self.altitude,
                        self.eccentricity,
                        self.inclination,
                        self.raan[street],
                        0,
                        positions[index],
                        focus=self.focus,
                        rads=False,
                    )
                )
        return satellites


class FlowerConstellation(Constellation):
    """
    Class for describing and holding a flower constellation of satellites

    Args:
        num_petals (PositiveInt): Number of petals formed when viewing the relative orbits
        num_days (PositiveInt): The number of days for the constellation to completely repeat its orbit
        num_satellites (PositiveInt): The desired number of satellites involved in the constellation
        phasing_n (PositiveInt): Phasing parameter n, effects allowable satellite positions
        phasing_d (PositiveInt): Phasing parameter d, effects the maximum number of satellites
        perigee_argument (float): Argument of perigee for satellite orbits [degrees]
        inclination (float): Inclination of orbit relative to equatorial plane [degrees]
        perigee_altitude (float): Altitude of perigee for satellite orbits [km]
        beam_width (float): Angular width of satellite sensor beam [degrees]
        focus (string): Object at focus of orbit, defaults to 'earth'
    """

    def __init__(
        self,
        num_petals,
        num_days,
        num_sats,
        phasing_n,
        phasing_d,
        perigee_argument,
        inclination,
        perigee_altitude,
        beam_width=1,
        raan_0=0,
        focus="earth",
    ):
        super(FlowerConstellation, self).__init__(
            num_sats, perigee_altitude, beam_width, 0, inclination, focus
        )

        flower_errors(num_petals, num_days, num_sats, phasing_n, phasing_d)

        self.num_petals = num_petals
        self.num_days = num_days
        self.phasing_n = phasing_n
        self.phasing_d = phasing_d
        self.perigee_argument = perigee_argument
        self.raan_0 = raan_0
        self.max_sats_per_orbit, self.max_sats = self.__calculate_max_satellites()
        self.raan_spacing, self.mean_anomaly_spacing = self.__calculate_spacing()
        self.num_orbits = self.phasing_d
        self.orbital_period, self.eccentricity, self.semi_major = self.__calculate_orbit_params()
        self.raan, self.mean_anomaly, self.true_anomaly = self.__calculate_orbits()

        self.revisit_time = self.__calculate_revisit_time()
        self.minimum_revisit_time = self.__calculate_minimum_revisit_time()
        self.satellites = self.__build_satellites()

    def __calculate_max_satellites(self) -> Tuple[float, float]:
        """
        Calculates the maximum number of satellites for the constellation
        """

        max_sats = self.phasing_d * self.num_days

        return self.num_days, max_sats

    def __calculate_spacing(self) -> Tuple[float, float]:
        """

        Calculate raan spacing and mean anomaly spacing between satellites
        """
        initial_raan_spacing = -360 * self.phasing_n / self.phasing_d
        initial_mean_anomaly_spacing = -1 * initial_raan_spacing * self.num_petals / self.num_days

        raan_spacing = normalise_angle(initial_raan_spacing)
        mean_anomaly_spacing = normalise_angle(initial_mean_anomaly_spacing)

        return raan_spacing, mean_anomaly_spacing

    def __calculate_orbits(self) -> Tuple[List[Union[float, int]], List[float], List[float]]:
        """

        Generates the right ascension and mean anomaly for each satellite in the constellation

        """
        raans = [self.raan_0]
        mean_anomalies = [0.0]
        true_anomalies = [0.0]

        # iterate over all (possible) satellites
        for index in range(1, min(self.max_sats, self.num_satellites)):
            # calculate right ascensions for this satellite
            raan_i = raans[index - 1] + self.raan_spacing
            raan_i = to_positive_angle(raan_i)
            raan_i = normalise_angle(raan_i)
            raans.append(raan_i)

            # calculate mean anomaly for each satellite
            mean_a = mean_anomalies[index - 1] + self.mean_anomaly_spacing
            mean_a = to_positive_angle(mean_a)
            mean_a = normalise_angle(mean_a)

            mean_anomalies.append(mean_a)

            # calculate eccentric anomaly, and use to find true anomaly for each sat
            eccentric_a = eccentricAnomaly(self.eccentricity, mean_a * math.pi / 180, 10, 100)
            true_a = trueAnomaly(self.eccentricity, eccentric_a, 10) * 180 / math.pi

            true_a = to_positive_angle(true_a)
            true_a = normalise_angle(true_a)

            true_anomalies.append(true_a)

        return raans, mean_anomalies, true_anomalies

    def __calculate_orbit_params(self) -> Tuple[float, float, float]:
        """

        Determines the perigee altitude, semi major axis and orbital period of the orbits

        """

        M = self.body_mass
        G = constants["G"]
        wE = constants["wE"]

        # orbital period
        T = (2 * math.pi / wE) * (self.num_days / self.num_petals)

        # semi-major axis, rearranged from formula for calculating orbital period from semi-major
        semi_major = math.pow(G * M * math.pow(T / (2 * math.pi), 2), 1 / 3)

        # eccentricity
        eccentric = 1 - 1000 * (self.body_radius + self.altitude) / semi_major

        return T, eccentric, semi_major

    def __calculate_revisit_time(self) -> float:
        """

        Determines time for satellite to revisit

        """

        revisit_time = self.num_days / self.num_satellites
        return revisit_time

    def __calculate_minimum_revisit_time(self) -> float:
        """

        Determines time for satellite to revisit given the maximum number of satellites

        """

        min_revisit_time = self.num_days / self.max_sats
        return min_revisit_time

    def __build_satellites(self) -> List[Satellite]:  # Convert to numpy
        """

        Creates a list of satellite objects based on the orbital parameters determined on initialisation

        """

        satellites = [
            newSatellite(
                self.altitude,
                self.eccentricity,
                self.inclination,
                self.raan[index],
                self.perigee_argument,
                self.true_anomaly[index],
                self.focus,
                rads=False,
            )
            for index in range(self.num_satellites)
        ]

        return satellites
