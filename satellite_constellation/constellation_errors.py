from satellite_constellation.constellation_exceptions import (
    AltitudeError,
    BeamError,
    ConstellationConfigurationError,
    ConstellationNumberError,
    ConstellationPlaneMismatchError,
    EccentricityError,
    FocusError,
    MaxSatellitesExceededError,
    PhaseError,
    RevisitError,
    StreetWidthError,
)
from satellite_constellation.utils import (
    heavenly_body_mass,
    heavenly_body_period,
    heavenly_body_radius,
)


def is_positive_int(toCheck):
    return not (toCheck < 1 or (toCheck % 1))


# General constellation errors
def constellation_errors(
    altitude,
    beam_width,
    eccentricity,
    focus="earth",
):
    if altitude < 0:
        raise AltitudeError("Negative altitude not allowed")
    elif altitude < 100:
        raise AltitudeError("Altitude below Karman line")

    if eccentricity < 0:
        raise EccentricityError("Invalid eccentricity. Eccentricity cannot be less than 0")
    elif eccentricity >= 1:
        raise EccentricityError("Invalid eccentricity. Parabolic/Hyperbolic trajectories not allowed")

    if beam_width <= 0 or beam_width > 180:
        raise BeamError("Beam width error. Must be within 0 to 180 degrees")

    if focus.lower() not in heavenly_body_radius:
        dictionary_error = "' not supported as a celestial body origin in util radius dictionary."
        raise FocusError("'" + focus.capitalize() + dictionary_error)

    if focus.lower() not in heavenly_body_mass:
        dictionary_error = "' not supported as a celestial body origin in util mass dictionary."
        raise FocusError("'" + focus.capitalize() + dictionary_error)

    if focus.lower() not in heavenly_body_period:
        dictionary_error = "' not supported as a celestial body origin in util body period dictionary."
        raise FocusError("'" + focus.capitalize() + dictionary_error)


# Walker specific constellation errors
def walker_errors(
    satellite_nums,
    satellite_planes,
    plane_phasing,
):
    if satellite_nums % satellite_planes:
        raise ConstellationPlaneMismatchError(
            "Number of satellites not compatible with planes in constellation"
        )

    if plane_phasing < 0:
        raise PhaseError("Negative number of phases not allowed")
    if plane_phasing % 1:
        raise PhaseError("Number of phases not an integer")
    if plane_phasing % satellite_planes == 0:
        raise PhaseError("Phase cannot equal number of planes times an integer constant")


# Flower specific constellation errors
def flower_errors(num_petals, num_days, num_sats, phasing_n, phasing_d):
    if num_sats > phasing_d * num_days:
        raise MaxSatellitesExceededError(
            "Number of satellites specified greater than maximum possible: {0}".format(phasing_d * num_days)
        )

    # test all inputs are positive integers
    for to_test in ["num_petals", "num_days", "num_sats", "phasing_n", "phasing_d"]:
        if not is_positive_int(locals()[to_test]):
            raise ConstellationNumberError(f"{to_test} must be a positive integer")


# Street specific constellation errors
def street_errors(num_streets, street_width, raan, revisit_time):
    if num_streets != len(raan):
        raise ConstellationConfigurationError(
            "Number of streets not compatible with number of right ascensions provided"
        )

    if not is_positive_int(num_streets):
        raise ConstellationNumberError("Invalid integer number of streets")

    if street_width > 180 or street_width <= 0:
        raise StreetWidthError("Street width error. Must be within 0 to 180 degrees")

    if revisit_time < 0:
        raise RevisitError("Revisit time cannot be less than 0 s")
