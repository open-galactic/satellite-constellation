"""
A class for creating a satellite object, describing the characteristics of it.
"""

from typing import List, TypedDict

import numpy as np

from satellite_constellation.frame_change import coe2rv
from satellite_constellation.utils import get_body_radius


def get_coes(sat) -> List[float]:
    """
    Formats the coes into a list for output
    Order of the COEs as found in Space Mission Engineering: The New SMAD (ISBN 978-1-881-883-15-9) pg 202
    and is as follows:

    Semimajor axis, eccentricity, inclination, arguemnt of perigee, right ascension of the ascending node,
    and true anomaly

    """

    a = sat["semi_major"]
    e = sat["eccentricity"]
    i = sat["inclination"]
    raan = sat["right_ascension"]
    aop = sat["aop"]
    true_anom = sat["true_anomaly"]

    # Order of the COEs as found in Space Mission Engineering: The New SMAD (ISBN 978-1-881-883-15-9) pg 202
    coes = [a, e, i, raan, aop, true_anom]

    return coes


def get_state_vectors(sat) -> List[List[float]]:
    """

    Calculates the ECEF state vectors for a satellite using COE
    :return: list of state vectors in the form [r, v] where r and v are lists representing position
     and velocity vectors
    """

    # Calculate state vectors from coes (returned in ECI)
    coe = np.array(get_coes(sat))
    r, v = coe2rv(coe)

    # convert to lists for output
    r_list, v_list = r.tolist(), v.tolist()

    # Return as list - [r, v]
    sat_list = [r_list, v_list]
    return sat_list


class Satellite(TypedDict, total=False):
    """
    Dictionary to represent a satellite, consisting of the satellite parameters, COEs and SVs

    Total flag must be false, since the state_vectors and coes won't be added to the dictionary
    until after the rest of the parameters, so the initialisation will not be total.
    """

    altitude: float
    focus: str
    true_alt: float
    eccentricity: float
    semi_major: float
    true_anomaly: float
    inclination: float
    right_ascension: float
    aop: float
    state_vectors: list
    coes: list


def newSatellite(
    alt: float,
    ecc: float,
    incl: float,
    raan: float,
    peri: float,
    ta: float,
    focus: str = "earth",
    rads: bool = True,
) -> Satellite:
    # represent satellite as a dictionary, supports radian or degree input

    # calculations for non-provided attributes
    semi_major = (alt + get_body_radius(focus)) / (1 - ecc)

    sat: Satellite = {
        "altitude": alt,
        "focus": focus.lower(),
        "true_alt": alt + get_body_radius(focus),
        "eccentricity": ecc,
        "semi_major": semi_major,
        # angle specific attributes
        "true_anomaly": ta if rads else np.radians(ta),
        "inclination": incl if rads else np.radians(incl),
        "right_ascension": raan if rads else np.radians(raan),
        "aop": peri if rads else np.radians(peri),
    }

    # calculate ECEF state vectors and COEs
    sat["state_vectors"] = get_state_vectors(sat)
    sat["coes"] = get_coes(sat)

    return sat
