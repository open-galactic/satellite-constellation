import numpy as np


def rv2coe(r, v, mu=398600):
    # Convert array of state vectors into classical orbital elements

    # Inputs must be two dimensional arrays, where each row is a different state vector
    if r.ndim == 1:
        r = np.array([r])
    if v.ndim == 1:
        v = np.array([v])

    rmag = np.sqrt(r[:, 0] ** 2 + r[:, 1] ** 2 + r[:, 2] ** 2)  # Magnitudes of the position vectors
    vmag = np.sqrt(v[:, 0] ** 2 + v[:, 1] ** 2 + v[:, 2] ** 2)  # Magnitudes of the velocity vectors

    # The radial velocity is given by dot(r,v)/rmag.
    # The line below performs this operation on multiple r and v
    # vectors at the same time
    vr = np.einsum("ij, ij->i", r, v) / rmag

    # Specific angular momentum vector and magnitude
    h = np.cross(r, v)
    hmag = np.sqrt(h[:, 0] ** 2 + h[:, 1] ** 2 + h[:, 2] ** 2)

    i = np.arccos(h[:, 2] / hmag)  # Find inclination

    # Calculate node vector, which points towards the ascending node
    N = np.cross(np.array([0, 0, 1]), h)
    Nmag = np.sqrt(N[:, 0] ** 2 + N[:, 1] ** 2 + N[:, 2] ** 2)

    # Eccentricity vector, which points towards periapsis and has magnitude equal to eccentricity of orbit
    e = (1 / mu) * ((vmag**2 - mu / rmag)[:, np.newaxis] * r - (rmag * vr)[:, np.newaxis] * v)
    emag = np.sqrt(e[:, 0] ** 2 + e[:, 1] ** 2 + e[:, 2] ** 2)

    # Length of semi-major axis
    a = hmag**2 / (mu * (1 - emag**2))

    raan = np.arccos(N[:, 0] / Nmag)  # Right ascension of the ascending node
    raan[N[:, 1] < 0] = 2 * np.pi - raan[N[:, 1] < 0]  # Make sure it's in the correct quadrant

    aop = np.arccos(np.einsum("ij, ij->i", N, e) / (Nmag * emag))  # Argument of periapsis
    aop[e[:, 2] < 0] = 2 * np.pi - aop[e[:, 2] < 0]  # Make sure it's in the correct quardant

    theta = np.arccos((1 / emag) * (hmag**2 / (mu * rmag) - 1))  # True anomaly
    theta[vr < 0] = 2 * np.pi - theta[vr < 0]  # Make sure it's in the correct quadrant

    THRESHOLD = 0.0005

    # Circular/Equatorial Case, ta, raan and aop are indistinguishible
    Eq_Circ_Ind = np.where((emag < THRESHOLD) & (i < THRESHOLD))
    x = np.repeat(np.array([1, 0, 0])[np.newaxis, :], len(i), axis=0)
    r_unit = r[Eq_Circ_Ind] / (rmag[Eq_Circ_Ind])[:, None]
    dot = (x[Eq_Circ_Ind] * r_unit[Eq_Circ_Ind]).sum(axis=1)
    theta[Eq_Circ_Ind] = np.arccos(dot[Eq_Circ_Ind])

    # Circular Case, ta and aop are indistinguishible
    Circ_Ind = np.where((emag < THRESHOLD) & (i > THRESHOLD))
    theta[Circ_Ind] = aop[Circ_Ind]
    aop[Circ_Ind] = 0

    # Equatorial Case, raan and aop are indistinguishible
    Eq_Ind = np.where((emag > THRESHOLD) & (i < THRESHOLD))
    w = np.arccos(e[:, 0] / np.sqrt(np.sum(np.square(e), axis=1)))
    aop[Eq_Ind] = w[Eq_Ind]

    result = np.column_stack([a, emag, i, raan, aop, theta])
    result[np.isnan(result)] = 0

    return result


def coe2rv(coe, mu=398600.4415):
    """Enter a numpy array of classical orbital elements a, e, i, raan, aop, theta (theta is true anomaly).
    All angles must be in radians. You can enter a 2 dimensional array,
    where each row is a different COE set."""
    # The array must be 2 dimensional

    # Set flag to convert to 2d and then back to one dimension later
    requires_conversion = coe.ndim == 1

    if requires_conversion:
        coe = np.array([coe])

    # Extract each orbital element from the array
    a = coe[:, 0]  # Semi-major axis
    e = coe[:, 1]  # Eccentricity
    i = coe[:, 2]  # Inclination
    raan = coe[:, 3]  # Right ascenscion of the ascending node
    aop = coe[:, 4]  # Argument of periapsis
    theta = coe[:, 5]  # True anomaly

    h = np.sqrt(mu * a * (1 - e**2))  # Specific angular momentum

    rpmag = h**2 / (mu * (1 + e * np.cos(theta)))  # Magnitude of position vector
    rp = np.column_stack([np.cos(theta), np.sin(theta), np.zeros(theta.size)])
    rp = rp * rpmag[:, np.newaxis]  # Position vector in perifocal frame

    vpmag = mu / h  # Magnitude of velocity vector
    vp = np.column_stack([-np.sin(theta), e + np.cos(theta), np.zeros(theta.size)])
    vp = vp * vpmag[:, np.newaxis]  # Velocity vector in perifocal frame

    # CHANGES START HERE

    # For the following cases,
    # If the orbit is both circular (e = 0) and equatorial (i = 0),
    # only true anomaly is needed to describe position
    QpX = np.repeat(np.identity(3)[np.newaxis, :, :], len(i), axis=0)

    # If the orbit is equatorial but not circular, a single angle
    # replaces both the right ascension and argument of
    # perigee
    Eq_Ind = np.where((e != 0) & (i == 0))
    QpX[Eq_Ind] = np.matmul(R3(aop[Eq_Ind]), R1(i[Eq_Ind]))

    # If the orbit is circular but not equatorial, a single angle
    # replaces the argument of perigee and true anomaly
    Circ_Ind = np.where((e == 0) & (i != 0))
    QpX[Circ_Ind] = np.matmul(R1(i[Circ_Ind]), R3(raan[Circ_Ind]))

    # If the orbit is neither circular or equatorial, all angle remain useful
    Gen_Ind = np.where((e != 0) & (i != 0))
    QpX[Gen_Ind] = np.matmul(np.matmul(R3(aop[Gen_Ind]), R1(i[Gen_Ind])), R3(raan[Gen_Ind]))

    # CHANGES END HERE

    # Transpose for matrix multiplication
    QXp = np.transpose(QpX, (0, 2, 1))

    # Adjust vectors to correct dimensions for matrix multiplication
    rp = rp[:, :, np.newaxis]
    vp = vp[:, :, np.newaxis]

    # Rotate state vector into ECI frame
    r = np.matmul(QXp, rp)
    v = np.matmul(QXp, vp)

    # Reshape state vectors into rows
    r = r.reshape(theta.size, 3)
    v = v.reshape(theta.size, 3)

    # if initial 1d array, convert back
    if requires_conversion:
        r = r[0]
        v = v[0]

    return r, v


# Rotation matrix about x axis
def R1(phi):
    """Enter an array of rotation angles in radians"""
    # The input must be a two dimensional array
    if np.isscalar(phi):
        phi = np.array([phi])

    if phi.ndim == 1:
        phi = np.array([phi])

    dcm = np.zeros([phi.size, 3, 3])
    dcm[:, 0, 0] = np.ones(phi.size)
    dcm[:, 1, 1] = np.cos(phi)
    dcm[:, 2, 2] = np.cos(phi)
    dcm[:, 1, 2] = np.sin(phi)
    dcm[:, 2, 1] = -np.sin(phi)

    return dcm


# Rotation matrix about y axis
def R2(phi):
    """Enter an array of rotation angles in radians"""
    # The input must be a two dimensional array
    if np.isscalar(phi):
        phi = np.array([phi])

    if phi.ndim == 1:
        phi = np.array([phi])

    dcm = np.zeros([phi.size, 3, 3])
    dcm[:, 1, 1] = np.ones(phi.size)
    dcm[:, 0, 0] = np.cos(phi)
    dcm[:, 2, 2] = np.cos(phi)
    dcm[:, 2, 0] = np.sin(phi)
    dcm[:, 0, 2] = -np.sin(phi)

    return dcm


# Rotation matrix about z axis
def R3(phi):
    """Enter an array of rotation angles in radians"""
    # The input must be a two dimensional array
    if np.isscalar(phi):
        phi = np.array([phi])

    if phi.ndim == 1:
        phi = np.array([phi])

    dcm = np.zeros([phi.size, 3, 3])
    dcm[:, 2, 2] = np.ones(phi.size)
    dcm[:, 0, 0] = np.cos(phi)
    dcm[:, 1, 1] = np.cos(phi)
    dcm[:, 0, 1] = np.sin(phi)
    dcm[:, 1, 0] = -np.sin(phi)

    return dcm
