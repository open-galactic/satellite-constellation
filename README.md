

# Constellation-Creator

Tool to generate satellite constellations based on constellation-level requirements.


Installation
-------------

To install latest released version::

    pip install satellite-constellation


To install from gitlab master branch::

    pip install https://gitlab.com/open-galactic/satellite-constellation


For development::

    # fork https://gitlab.com/open-galactic/satellite-constellation to YOUR_GITHUB
    # clone your repo locally
    git clone https://gitlab.com/open-galactic/satellite-constellation
    cd satellite-constellation

    # add upstream remote
    git remote add upstream https://gitlab.com/open-galactic/satellite-constellation

    # create a virtualenv and install for development
    python3 -m venv venv
    source venv/bin/activate
    pip install --upgrade pip
    pip install -r requirements.txt

Usage
-------

The 'constellation.py' file provides three classes, WalkerConstellation(), FlowerConstellation() and SOCConstellation() to represent a Walker Delta constellation, a Flower constellation, and a Streets of Coverage constellation respectively.

The object creation parameters are as follows:

### Walker Constellation
    WalkerConstellation(num_sats, phasing, inclination, altitude, eccentricity, beam_width, focus='Earth')

where:
- `num_planes`: The number of different orbit planes in the constellation "p"
- `phasing`: Dictates the spacing between equivalent satellites in neighbouring orbital planes "f"
- `inclination`: Inclination of orbit relative to equatorial plane "i" [degrees]
- `altitude`: Altitude of satellites in orbit [km]
- `eccentricity`: Eccentricity of satellite orbits
- `beam_width`: Sensor/instrument beam width of the satellites [degrees]
- `focus`: Object at focus of orbit, defaults to 'earth'

### Flower Constellation
    FlowerConstellation(num_petals, num_days, num_satellites, phasing_n, phasing_d, perigee_argument, inclination, perigee_altitude, beam_width, focus='Earth')

where:
- `num_petals`: Number of petals formed when viewing the relative orbits
- `num_days`: The number of days for the constellation to completely repeat its orbit
- `num_satellites`: The desired number of satellites involved in the constellation
- `phasing_n`: Phasing parameter n, effects allowable satellite positions
- `phasing_d`: Phasing parameter d, effects the maximum number of satellites
- `perigee_argument`: Argument of perigee for satellite orbits [degrees]
- `inclination`: Inclination of orbit relative to equatorial plane [degrees]
- `perigee_altitude`: Altitude of perigee for satellite orbits [km]
- `beam_width`: Angular width of satellite sensor beam [degrees]
- `focus`: Object at focus of orbit, defaults to 'earth'

### Streets of Coverage Constellation
    SOCConstellation(num_streets, street_width, altitude, beam_width, raan, eccentricity, revisit_time, focus='earth')

where:
- `num_streets`: Number of satellites used in the constellation
- `street_width`: Angular width of street [degrees]
- `altitude`: Altitude of satellites in orbit [km]
- `beam_width`: Sensor beam width of the satellites [degrees]
- `raan`: List of right ascension for each street [degrees]
- `eccentricity`: Eccentricity of satellite orbits
- `revisit_time`: Time until latitude re-enters satellite coverage [s]
- `focus`: Heavenly body at the focus of the orbit, defaults to 'Earth'


Instantiating a class with the appropriate requirements will perform the required calculations and return an object representing 
the constellation, from which two functions can be called:

`.as_state_vectors()`: a list of the satellites within this constellation, each represented as a list
                     of 2 vectors [position (r) and velocity (v)] themselves lists representing the values.


                myWalkerObject.as_state_vectors() = [ 
                            [ [r_x, r_y, r_z], [v_x, v_y, v_z] ], # satellite 1
                            [ [r_x, r_y, r_z], [v_x, v_y, v_z] ], # satellite 2
                            [ [r_x, r_y, r_z], [v_x, v_y, v_z] ], # satellite 3
                        
                        ]

`.as_coes()`: a list of the satellites within this constellation, each represented as a list of the Keplerian Classical Orbital Elements in the order: [semimajor axis, eccentricity, inclination, right ascension, argument of periapsis, true anomaly].
                    
                myWalkerObject.as_coes() = [ 
                            [a, e, i, raan, aop, theta], # satellite 1
                            [a, e, i, raan, aop, theta], # satellite 2
                            [a, e, i, raan, aop, theta], # satellite 3
                        ]

